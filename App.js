import React, { useEffect, useState } from 'react';
import TrackPlayer from 'react-native-track-player';
import { AppNavigation } from './src/navigation/AppNavigation';
import trackPlayerServices from './src/services/track-player.service';
import { TrackPlayerContextProvider } from './src/context/TrackPlayerContex';

const track = {
  id: '1',
  url:
    'https://d12npcgzwq7lzc.cloudfront.net/Programs/NEW!%20In%20Out%20Through%20Vol.%20I/01%20Expansion%20of%20the%20Self%20-%20SAMPLE.mp3?Expires=1606585609&Key-Pair-Id=APKAJXWBMWFPRW43RNRQ&Signature=K6F4zGxhfMg6GE3jubLaZL3hUzbDpKkOVtQUqq0-Q5XebpzpfqQNmV5Kf9HasBwUMLoavj9BIkH1Rux5IiMT~jAK7SUJEWabnmnmgxTq7Yjf3gC~Q6SUEbq6Ut2-qu1MJ83CP3uZa~9tpOW23H80u3R6DYdwnOXcN~ionz-bMLmFtsEAD~2v24L1j28G43uZq6OQ4xBImab3xDqN5gkG5lfgzXAldWIcoEddwW5v8I6rxKvI3-RDKEWqvewuWW3ZcP6XQzqDNHZIPTBWP-oAE7upU2VZMmvBVBUVKovYJhpuuGRj2FVWlq9HxdzAeHAPFZBUnI88O6rkKsDX-KkrFw__',
  title: 'New TRACK',
  artist: 'ARTIST',
};

const App = () => {

  useEffect(() => {
    TrackPlayer.setupPlayer().then(() => {
      TrackPlayer.registerPlaybackService(() => trackPlayerServices);
    });
  }, []);

  return (
    <TrackPlayerContextProvider>
      <AppNavigation />
    </TrackPlayerContextProvider>
  );
};

export default App;
