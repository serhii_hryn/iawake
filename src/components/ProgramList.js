import React, { useState, useEffect } from 'react';
import { View, StyleSheet, ScrollView, RefreshControl } from 'react-native';
import { mediaLibraryService } from '../services/media-library.service';
import { ProgramCard } from './ProgramCard';

export const ProgramList = () => {
  const [programs, setPrograms] = useState([]);

  const [refreshing, setRefreshing] = React.useState(false);

  const fetchFreeMediaLibrary = async () => {
    setRefreshing(true);
    const freeMediaLibrary = await mediaLibraryService.getFreeMediaLibrary();
    setRefreshing(false);

    setPrograms(freeMediaLibrary.programs || []);
  };

  useEffect(() => {
    fetchFreeMediaLibrary();
  }, []);

  return (
    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={fetchFreeMediaLibrary}
        />
      }
      style={styles.container}>
      <View style={styles.programList}>
        {programs.map((program) => (
          <ProgramCard key={program.id} program={program} />
        ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  programList: {
    height: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
});
