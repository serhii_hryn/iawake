import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { useTrackPlayerContext } from '../context/TrackPlayerContex';
import Icon from 'react-native-vector-icons/Feather';

export const Player = () => {
  const playerContext = useTrackPlayerContext();

  if (playerContext.isEmpty || !playerContext.currentTrack) {
    return null;
  }

  return (
    <View style={styles.player}>
      <View>
        <Text>{playerContext.currentTrack.title}</Text>
      </View>
      <View>
        {playerContext.isBuffering && <ActivityIndicator color="#000" />}
        {playerContext.isPaused && (
          <TouchableOpacity onPress={() => playerContext.play()}>
            <Icon name="play" size={30} />
          </TouchableOpacity>
        )}

        {playerContext.isPlaying && (
          <TouchableOpacity onPress={playerContext.pause}>
            <Icon name="pause" size={30} />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  player: {
    height: 75,
    borderTopColor: '#ccc',
    borderTopWidth: 1,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    backgroundColor: 'white',
  },
});
