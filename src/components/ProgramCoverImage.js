import React from 'react';
import { Image } from 'react-native';

export const ProgramCoverImage = ({ uri, styles }) => {
  return (
    <Image
      defaultSource={require('../resources/images/music_placeholder.png')}
      source={{ uri }}
      style={{ ...styles }}
    />
  );
};
