import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ProgramCoverImage } from './ProgramCoverImage';

export const ProgramCard = ({ program }) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={() => navigation.navigate('Program', { program })}>
      <View style={styles.card}>
        <ProgramCoverImage uri={program.cover.url} styles={styles.cover} />
        <Text style={styles.title}>{program.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    width: 150,
    height: 200,
    alignItems: 'center',
    marginHorizontal: 10,
    marginBottom: 10,
  },
  cover: {
    width: 150,
    height: 150,
    marginBottom: 5,
    borderRadius: 10,
  },

  title: {
    fontWeight: '500',
  },
});
