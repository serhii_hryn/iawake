import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useRoute } from '@react-navigation/native';
import { TrackList } from './TrackList';
import { ProgramCoverImage } from './ProgramCoverImage';
import { Player } from './Player';

export const ProgramDetails = () => {
  const route = useRoute();
  const { program } = route.params;

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <ProgramCoverImage uri={program.cover.url} styles={styles.cover} />
        <View style={styles.headerText}>
          <Text style={styles.title}>{program.title}</Text>
          <Text style={styles.trackCount}>
            {program.tracks.length}
            {program.tracks.length === 1 ? ' track' : ' tracks'}
          </Text>
        </View>
      </View>
      <View style={styles.content}>
        <TrackList tracks={program.tracks} />
        <Player />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  headerText: {
    flexGrow: 1,
    flex: 1,
  },
  cover: {
    width: 120,
    height: 120,
    marginRight: 20,
    borderRadius: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: '500',
  },
  trackCount: {
    fontSize: 12,
  },
  content: {
    flex: 1,
    justifyContent: 'space-between',
  },
});
