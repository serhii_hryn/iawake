import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import { TrackItem } from './TrackItem';

export const TrackList = ({ tracks }) => {
  return (
    <View style={styles.tracks}>
      <FlatList
        data={tracks}
        renderItem={({ item, index }) => (
          <TrackItem track={item} trackNumber={index + 1} />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  tracks: {
    marginTop: 5,
    marginHorizontal: 10,
  },
});
