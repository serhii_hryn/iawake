import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { formatSeconds } from '../utils';
import { useTrackPlayerContext } from '../context/TrackPlayerContex';

export const TrackItem = ({ track, trackNumber }) => {
  const playerContext = useTrackPlayerContext();

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={() => {
        playerContext.play({
          title: track.title,
          id: track.key,
          url: track.media.mp3.url,
          artist: 'Track Artist',
          artwork: require('../resources/images/music_placeholder.png'),
        });
      }}>
      <View style={styles.trackItem}>
        <View>
          <Text>
            {trackNumber}. {track.title}
          </Text>
        </View>
        <View>
          <Text>{formatSeconds(track.duration)}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  trackItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 25,
    backgroundColor: '#e0e0e0',
    marginVertical: 2,
  },
});
