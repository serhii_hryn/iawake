import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../screens/HomeScreen';
import { ProgramScreen } from '../screens/ProgramScreen';

const Stack = createStackNavigator();

export const AppNavigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Programs" component={HomeScreen} />
        <Stack.Screen
          name="Program"
          component={ProgramScreen}
          options={{ title: 'Program Details' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
