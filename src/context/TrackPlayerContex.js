import React, { useState, useEffect } from 'react';
import TrackPlayer, {
  STATE_PLAYING,
  STATE_PAUSED,
  STATE_STOPPED,
  STATE_BUFFERING,
} from 'react-native-track-player';

export const TrackPlayerContext = React.createContext({
  isPlaying: true,
  isPaused: false,
  isStopped: false,
  isBuffering: false,
  isEmpty: true,
  currentTrack: null,
  play: () => null,
  pause: () => null,
  stop: () => null,
});

export const TrackPlayerContextProvider = (props) => {
  const [playerState, setPlayerState] = useState(null);
  const [currentTrack, setCurrentTrack] = useState(null);

  useEffect(() => {
    const listener = TrackPlayer.addEventListener(
      'playback-state',
      ({ state }) => {
        setPlayerState(state);
      },
    );

    return () => {
      listener.remove();
    };
  }, []);

  const play = async (track) => {
    if (!track) {
      if (currentTrack) {
        await TrackPlayer.play();
      }
      return;
    }

    await TrackPlayer.add([track]);
    setCurrentTrack(track);
    await TrackPlayer.play();
  };

  const pause = async () => {
    await TrackPlayer.pause();
  };

  const value = {
    isPlaying: playerState === STATE_PLAYING,
    isPaused: playerState === STATE_PAUSED,
    isStopped: playerState === STATE_STOPPED,
    isBuffering: playerState === STATE_BUFFERING,
    isEmpty: playerState === null,
    currentTrack,
    pause,
    play,
  };

  return (
    <TrackPlayerContext.Provider value={value}>
      {props.children}
    </TrackPlayerContext.Provider>
  );
};

export const useTrackPlayerContext = () => React.useContext(TrackPlayerContext);
