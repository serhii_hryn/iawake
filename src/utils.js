import { Platform } from 'react-native';

export const log = (msg) => console.log(`[${Platform.OS}]: ${msg}`);

export const formatSeconds = (s) => {
  const minutes = Math.floor(s / 60);
  let seconds = Math.floor(s - minutes * 60);
  seconds = seconds.toString().length === 1 ? '0' + seconds : seconds;

  return `${minutes}:${seconds}`;
};
