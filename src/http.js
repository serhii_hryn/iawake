import axios from 'axios';
import Config from 'react-native-config';

const IAWAKE_API_URL = Config.IAWAKE_API_URL;

export const iAwakeClient = () => {
  return axios.create({
    baseURL: IAWAKE_API_URL,
  });
};
