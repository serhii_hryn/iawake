import { iAwakeClient } from '../http';

export const mediaLibraryService = {
  async getFreeMediaLibrary() {
    return await (await iAwakeClient().get('/media-library/free')).data;
  },
};
