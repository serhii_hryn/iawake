import React from 'react';
import { View, SafeAreaView } from 'react-native';
import { ProgramDetails } from '../components/ProgramDetails';

export const ProgramScreen = () => {
  return (
    <SafeAreaView>
      <View>
        <ProgramDetails />
      </View>
    </SafeAreaView>
  );
};
